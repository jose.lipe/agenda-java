package agenda;

import java.util.ArrayList;

public class Pessoa {
	private int id;
	private String nome;
	private ArrayList<Endereco> enderecos;
	private ArrayList<Telefone> telefones;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void addEndereco(int id, String rua, String numero, String complemento, String bairro, String tipo, Cidade cidade) {
		Endereco endereco = new Endereco();
		endereco.setId(id);
		endereco.setRua(rua);
		endereco.setNumero(numero);
		endereco.setComplemento(complemento);
		endereco.setBairro(bairro);
		endereco.setTipo(tipo);
		endereco.setCidade(cidade);
		this.enderecos.add(endereco);
	}
	
	public ArrayList<Endereco> getEndereco() {
		return this.enderecos;
	}
	public void addTelefone(int id, String numero, String tipo) {
		Telefone telefone = new Telefone();
		telefone.setId(id);
		telefone.setNumero(numero);
		telefone.setTipo(tipo);
		this.telefones.add(telefone);
	}
	
	public ArrayList<Telefone> getTelefone() {
		return this.telefones;
	}
}
